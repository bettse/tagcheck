const os = require('os');
const noble = require('@abandonware/noble');

const logError = err => err && console.log.err(err);

const findmySoundService = 'fd44';
const airtagSoundService = '7DFC90007D1C495186AA8D9728F8D66C';

const airtagSoundChar = '7dfc90017d1c495186aa8d9728f8d66c';
const findmySoundChar = '4f860003943b49efbed42f730304427a';

const findingTypeAndLength = Buffer.from('1219', 'hex');
const appleCompanyId = Buffer.from('4c00', 'hex');

const startSound = Buffer.from([0xAD])
const callback = 0x302
let watchdog;

async function onPeripheral(peripheral) {
  const {advertisement, uuid, connectable} = peripheral;
  const {manufacturerData} = advertisement;
  if (manufacturerData === undefined) {
    return;
  }
  if (!connectable) {
    return;
  }
  if(watchdog) {
    console.log('connection in progress');
    return;
  }

  if (manufacturerData.slice(0, 2).equals(appleCompanyId)) {
    console.log('scanning...', uuid, manufacturerData.toString('hex'))
    if (manufacturerData.slice(2, 4).equals(findingTypeAndLength)) {
      try {
        await noble.stopScanningAsync();
        console.log(`Connecting`, uuid, manufacturerData[4].toString(0x10));
        let cancelled = false;
        watchdog = setTimeout(() => {
          console.log('timeout, re-starting scan');
          cancelled = true;
          noble.startScanningAsync([], true, logError);
        }, 10000);
        await peripheral.connectAsync();
        if (cancelled) {
          return;
        }
        clearTimeout(watchdog);
        watchdog = null;

        const { characteristics } = await peripheral.discoverSomeServicesAndCharacteristicsAsync([]);

        characteristics.forEach(async (characteristic) => {
          const { properties, uuid } = characteristic;
          if (uuid.length > 4) {
            console.log({uuid: uuid.slice(4, 8), properties})
          } else {
            console.log({uuid, properties})
          }
          const withoutResponse = properties.includes('writeWithoutResponse');
          if (uuid.length === 4) {
            if(properties.includes('notify')) {
              characteristic.on('data', async (data, isNotification) => {
                console.log({characteristic, data})
              })
              characteristic.subscribe(logError);
            }
            if (properties.includes('read')) {
              const data = await characteristic.readAsync()
              console.log({uuid, data})
            }
          }
          switch(uuid) {
            case airtagSoundChar:
              console.log('startSound')
              characteristic.write(startSound, withoutResponse, (error) => {
                if (error) {
                  console.log('write error', error);
                  return
                }
                console.log('written');

                peripheral.disconnect()
                noble.startScanningAsync([], true, logError);
              })
              break;
            case findmySoundChar:
              console.log('TBD')
              characteristic.on('data', async (data, isNotification) => {
                console.log({data})
              })
              characteristic.subscribe(logError);
            break;
          }
        })
      } catch (e) {
        console.log('oh shit', e)
        if (e === 'Could not find all requested services') {
          noble.startScanningAsync([], true, logError);
        }
      }
    }
  }
}

function main() {
  noble.on('stateChange', async state => {
    if (state === 'poweredOn') {
      await noble.startScanningAsync([], true, logError);
    } else {
      console.log(`stateChange: ${state}`);
    }
  });

  noble.on('discover', onPeripheral);
}

main();
